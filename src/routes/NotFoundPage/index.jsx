import AppBody from "components/AppBody";
import Header from "components/Sections/Header";
import Footer from "components/Sections/Footer";

import "./index.scoped.css";

function NotFoundPage() {

    const headerNavigation = [
        {title: "Home", link: "#home"},
        {title: "About", link: "#about"},
        {title: "Portfolio", link: "#portfolio"},
        {title: "Contact", link: "#contact"}
    ];

    return (
        <AppBody>
            <div id="home"></div>
            <Header navigation={headerNavigation} />
            <div className="block__wrapper">
            <div class="container">
                    <h1>404</h1>
                    <p>
                        Something was wrong, <br /> seems, that this page is not exist
                    </p>
                    <small>Try to backward on <a href={`${process.env.PUBLIC_URL}`}>home page</a></small>
                    <div class="circle small"></div>
                    <div class="circle medium"></div>
                    <div class="circle big"></div>
                </div>

            </div>
            {/* <Footer /> */}
        </AppBody>
    );
}

export default NotFoundPage;
