import AppBody from "components/AppBody";
import Header from "components/Sections/Header";
import HomeSection from "components/Sections/HomeSection";
import AboutSection from "components/Sections/AboutSection";
import ResumeSection from "components/Sections/ResumeSection";
import EducationSection from "components/Sections/EducationSection";
import ProjectSection from "components/Sections/ProjectSection";
import Footer from "components/Sections/Footer";

import "./index.scoped.css";

function MainPage() {

    const headerNavigation = [
        {title: "Home", link: "#home"},
        {title: "About", link: "#about"},
        {title: "Resume", link: "#resume"},
        {title: "Projects", link: "#project"}
    ];

    
    return (
        <AppBody>
            <div id="home"></div>
            <Header navigation={headerNavigation} />
            <HomeSection />
            <AboutSection />
            <ResumeSection />
            <div className="separator"></div>
            <EducationSection />
            <ProjectSection />
            {/* <Footer /> */}
        </AppBody>
    );

}

export default MainPage;
