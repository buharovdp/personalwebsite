import AppContainer from "components/AppContainer";
import { TypeAnimation } from 'react-type-animation';

import cv from "./CV.png"

import "./index.scoped.css";

function AboutSection() {
    return (
        <section id="about" className="about anim">
            <AppContainer>
                <div className="about__container">

                    <div className="about__image"></div>
                    <div className="about__content">
                        <h2>About me</h2>
                        <div className="stack">
                            <TypingText />
                        </div>
                        <p className="about__text">
                            I am a student at ITMO University, studying software engineering.
                            I own a large number of technologies,
                            I have experience writing various types of software,
                            starting with python script, ending with microservice
                            architecture and drivers for Linux. In my free time,
                            I chat with friends, hang out at games, or draw another
                            masterpiece in an illustrator.
                        </p>

                        <div className="separator"></div>

                        <h2>Contact Details</h2>

                        <div className="contact__wrapper">
                            <div className="text">
                                <p>
                                    Dmitry Buharov
                                </p>

                                <p>
                                    Russia Moscow/Saint Petersburg
                                </p>

                                <a className="contact__info" href="mailto:buharovdp@gmail.com">buharovdp@gmail.com</a>
                                <a className="contact__info" href="tel:+7 926 464 75 72">+7 926 464 75 72</a>
                                <a className="contact__info" href="https://t.me/DmitryBuharov">@buharovdp</a>

                                <a href={cv} class="button">Download CV
                                    <svg class="button__icon" width="800px" height="800px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" data-v-2caf9176=""><path d="M3 15C3 17.8284 3 19.2426 3.87868 20.1213C4.75736 21 6.17157 21 9 21H15C17.8284 21 19.2426 21 20.1213 20.1213C21 19.2426 21 17.8284 21 15" stroke="var(--lightTextColor)" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" data-v-2caf9176=""></path><path d="M12 3V16M12 16L16 11.625M12 16L8 11.625" stroke="var(--lightTextColor)" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" data-v-2caf9176=""></path>
                                    </svg>
                                </a>
                            </div>
                        </div>


                    </div>
                </div>

            </AppContainer>
        </section>
    );
}


const TypingText = () => {
    return (
        <TypeAnimation
            sequence={[
                'My Stack: Golang, Java, Python, JavaScript, Kotlin, C',
                3000,
                'My Stack: gRPC, Spring, React',
                3000,
                'My Stack: Postgres, MongoDB, Kafka, Redis',
                3000,
                'My Stack: Docker, Bash, Linux, Git',
                3000,
                'My Stack: Prometheus, Grafana, Jaeger',
                3000,
                'My Stack: UnitTests, Selenium, JMeter'
            ]}
            cursor={false}
            speed={50}
            style={{ fontSize: '1.4rem', display: 'inline-block' }}
            repeat={Infinity}
        />
    );
};

export default AboutSection;