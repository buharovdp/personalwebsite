import AppContainer from "components/AppContainer";

import "./index.scoped.css";
import ProjectCard from "components/ProjectCard";

function ProjectSection() {
    return (
        <section id="project" className="project anim">
            <AppContainer>
                <div className="project__container">

                    <h3 className="project__title">
                        Projects
                    </h3>
                    <div className="project__content">
                        <ProjectCard
                            title="NoteIt"
                            description="A convenient note/todolist system available on all devices"
                            link="https://gitlab.com/buharovdp/noteit"
                        >
                            <div className="card lang"><p>Golang</p></div>
                            <div className="card lang"><p>ReactJS</p></div>
                            <div className="card data"><p>Postgres</p></div>
                            <div className="card data"><p>MongoDB</p></div>
                            <div className="card architecture"><p>Microservices</p></div>
                            <div className="card architecture"><p>Rest</p></div>
                        </ProjectCard>

                        <div className="separator"></div>

                        <ProjectCard
                            title="CookIt"
                            description="Mobile application with recipes that won the IT Samsung School"
                            link="https://gitlab.com/buharovdp/cookit"
                        >
                            <div className="card lang"><p>Java</p></div>
                            <div className="card data"><p>LiteSQL</p></div>
                            <div className="card architecture"><p>Mobile</p></div>
                            <div className="card addition"><p>Android</p></div>
                        </ProjectCard>

                        <div className="separator"></div>

                        <ProjectCard
                            title="Delivery Point"
                            description="gRPC learning service that uses a lot of cool stuff"
                            link="https://gitlab.com/buharovdp/deliverypoint"
                        >
                            <div className="card lang"><p>Golang</p></div>
                            <div className="card data"><p>Postgres</p></div>
                            <div className="card architecture"><p>gRPC</p></div>
                            <div className="card architecture"><p>Microservices</p></div>
                            <div className="card addition"><p>Prometheus</p></div>
                            <div className="card addition"><p>Jaeger</p></div>
                            <div className="card addition"><p>Docker</p></div>
                        </ProjectCard>

                        <div className="separator"></div>

                        <ProjectCard
                            title="HomeIt"
                            description="The emulator of smart homes, and the implementation of a mobile application. The service was written in a small team"
                            link="https://gitlab.com/buharovdp/homeit"
                        >
                            <div className="card lang"><p>Kotlin</p></div>
                            <div className="card data"><p>Postgres</p></div>
                            <div className="card data"><p>Redis</p></div>
                            <div className="card architecture"><p>gRPC</p></div>
                            <div className="card architecture"><p>Microservices</p></div>
                            <div className="card addition"><p>Docker</p></div>
                        </ProjectCard>
                    </div>

                </div>
            </AppContainer >
        </section >
    );
}

export default ProjectSection;