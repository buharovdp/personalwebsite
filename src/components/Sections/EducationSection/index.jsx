import AppContainer from "components/AppContainer";
import { TypeAnimation } from 'react-type-animation';

import TimeCard from "components/TimeCard";

import "./index.scoped.css";

function EducationSection() {
    return (
        <section id="education" className="education anim">
            <AppContainer>
                <div className="education__container">

                    <h3 className="education__title">
                        Education
                    </h3>
                    <div className="education__content">

                        <TimeCard
                            title="Yandex Lyceum"
                            role="Python course"
                            years="September 2019 — May 2021">
                            <p>
                            I graduated from the Yandex. python course with honors. On it, I learned the basics of programming, got acquainted with OOP, algorithms and data structures
                            </p>
                        </TimeCard>

                        <div className="separator"></div>

                        <TimeCard
                            title="Samsung IT School"
                            role="Java mobile development course"
                            years="September 2020 — May 2021">
                            <p>
                            Also graduated from the Samsung school courses with honors. In a team with a friend, we developed a mobile application in Java, and took 2nd place at the all-Russian competition "Samsung School chooses the strongest". During the course, I learned the basics of Java, got acquainted with multithreading, team development and other cool things.
                            </p>
                        </TimeCard>

                        <div className="separator"></div>

                        <TimeCard
                            title="ITMO University"
                            role="B.A. System and application software"
                            years="September 2021 — Present">
                            <p>
                            At the moment, I am studying in the third year of the System and application software. I have an average GPA of 4.77 out of 5 for the first year and 4.8 for the second one.
                            </p>
                        </TimeCard>
                        
                    </div>

                </div>
            </AppContainer >
        </section >
    );
}


const TypingText = () => {
    return (
        <TypeAnimation
            sequence={[
                'My Stack: Golang, Java, Python, JavaScript, Kotlin, C',
                3000,
                'My Stack: gRPC, Spring, React',
                3000,
                'My Stack: Postgres, MongoDB, Kafka, Redis',
                3000,
                'My Stack: Docker, Bash, Linux, Git',
                3000,
                'My Stack: Prometheus, Grafana, Jaeger',
                3000,
                'My Stack: UnitTests, Selenium, JMeter'
            ]}
            cursor={false}
            speed={50}
            style={{ fontSize: '1.4rem', display: 'inline-block' }}
            repeat={Infinity}
        />
    );
};

export default EducationSection;