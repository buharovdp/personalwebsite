import AppContainer from "components/AppContainer";

import "./index.scoped.css";

function Footer(props) {

    return (
        <div className="footer anim">
            <AppContainer>

                <div className="footer__container">
                    <div className="empty"></div>
                    <div className="copyright">&copy; Copyright {new Date().getFullYear()} buharovdp.ru</div>
                    <div className="author__block">
                        <p>by Dmitry Buharov<br />slovenlypolygon group</p>
                        <img className="footer__logo__image" />
                    </div>
                </div>

            </AppContainer>
        </div>
    );

}

export default Footer;
