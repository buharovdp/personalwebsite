import AppContainer from "components/AppContainer";
import Data from "./Data";
import ScrollDown from "components/ScrollDown";

import "./index.scoped.css";

function HomeSection(props) {

    return (
        <section className="home">
            <AppContainer>

                <div className="home__container grid">
                    <div className="home__content grid anim">
                        <div className="home__img"></div>
                        <Data />
                    </div>
                </div>

                <a href="#about" className="scroll__wrapper">
                    <ScrollDown />
                </a>

            </AppContainer>
        </section>
    );

}

export default HomeSection;
