import { TypeAnimation } from 'react-type-animation';
import { FaTelegram, FaGitlab, FaEnvelope } from "react-icons/fa6";
import { IconContext } from "react-icons";

import "./index.scoped.css";

function Data() {
    return (
        <div className="home__data">
            <h1 className="home__title">Buharov Dmitry
                {/* <img className="greeting__logo"></img> */}
            </h1>
            <h3 className="home__subtitle">
                <TypingText />
            </h3>
            <p className="home__description">I am a software developer who enjoy 
            coding, algorithms, minimalism and vector graphic. 
            Let's start <a className="text__link" href="#about">scrolling </a> 
            and learn more <a className="text__link" href="#about">about me</a>.
            </p>
                        
            <div className="links">


                <IconContext.Provider value={{ className: "tg" }}>
                    <a className="link" href="https://t.me/buharovdp">
                        <FaTelegram />
                    </a>
                </IconContext.Provider>
                <IconContext.Provider value={{ className: "gt" }}>
                    <a href="https://gitlab.com/buharovdp">
                        <FaGitlab />
                    </a>
                </IconContext.Provider>
                <IconContext.Provider value={{ className: "ml" }}>
                    <a href="buharovdp@gmail.com">
                        <FaEnvelope />
                    </a>
                </IconContext.Provider>

                <a href="https://www.kaggle.com/dmitryellison"><i class="fab fa-kaggle" aria-hidden="true"></i></a>
            </div>
        </div>
    );
}

const TypingText = () => {
    return (
        <TypeAnimation
            sequence={[
                'Software Developer💻',
                2000,
                'Illustrator enjoyer✒️',
                2000,
                'ITMO Student🏫',
                2000,
                'Just Slovy🐦',
                2000
            ]}
            cursor={false}
            speed={50}
            style={{ fontSize: '1em', display: 'inline-block' }}
            repeat={Infinity}
        />
    );
};

export default Data;
