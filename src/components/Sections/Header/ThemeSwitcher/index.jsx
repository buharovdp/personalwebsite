import { useEffect, useRef } from "react";

import "./index.scoped.css";

function ThemeSwitcher() {

    const THEMES = ["light__theme", "dark__theme"];
    const checkbox = useRef();

    useEffect(() => {
        var html = document.getElementsByTagName("html");
        var radios = document.getElementsByName("themes");
        var gradient = document.getElementById("gradient__theme");

        if (!THEMES.includes(localStorage.getItem("theme"))) {
            html[0].classList.add("light__theme");
            localStorage.setItem("theme", "light__theme");
        } else {
            html[0].classList.add(localStorage.getItem("theme"));
            document.getElementById(localStorage.getItem("theme")).checked = true;
        }

        if (localStorage.getItem("gradient") == null || localStorage.getItem("gradient") === "grad__off") {
            localStorage.setItem("gradient", "grad__off")
        } else {
            html[0].classList.add(localStorage.getItem("gradient"));
            checkbox.current.checked = true;
        }

        for (let i = 0; i < radios.length; i++) {
            radios[i].addEventListener('change', function () {
                html[0].classList.remove("light__theme");
                html[0].classList.remove("dark__theme");
                html[0].classList.add(this.id);
                localStorage.setItem("theme", this.id)
            });
        }
    }, []);

    function gradientSwitch() {
        var html = document.getElementsByTagName("html");

        if (checkbox.current.checked) {
            localStorage.setItem("gradient", "gradient")
            html[0].classList.remove("grad__off");
            html[0].classList.add("gradient");
        } else {
            localStorage.setItem("gradient", "grad__off")
            html[0].classList.remove("gradient");
        }
    }


    return (
        <div className="switcher__wrapper">
            <div className="theme__switcher">
                <input type="radio" id="light__theme" name="themes" />
                <label htmlFor="light__theme">
                    <span className="light">
                        {/* <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M16,24a8,8,0,1,1,8-8A8,8,0,0,1,16,24ZM16,9.66A6.34,6.34,0,1,0,22.34,16,6.35,6.35,0,0,0,16,9.66Z" fill="var(--primary)" /><path d="M16,6.8A.82.82,0,0,1,15.18,6V2.82a.82.82,0,0,1,1.64,0V6A.82.82,0,0,1,16,6.8Z" fill="var(--primary)" /><path d="M16,30a.82.82,0,0,1-.82-.82V26a.82.82,0,1,1,1.64,0v3.16A.82.82,0,0,1,16,30Z" fill="var(--primary)" /><path d="M29.18,16.82H26a.82.82,0,1,1,0-1.64h3.16a.82.82,0,0,1,0,1.64Z" fill="var(--primary)" /><path d="M6,16.82H2.82a.82.82,0,1,1,0-1.64H6a.82.82,0,0,1,0,1.64Z" fill="var(--primary)" /><path d="M23.09,9.74a.87.87,0,0,1-.59-.25.82.82,0,0,1-.24-.58.79.79,0,0,1,.25-.58L24.73,6.1a.83.83,0,0,1,1.17,0,.82.82,0,0,1,0,1.16L23.67,9.49A.83.83,0,0,1,23.09,9.74Z" fill="var(--primary)" /><path d="M6.68,26.14a.82.82,0,0,1-.82-.82.86.86,0,0,1,.24-.59l2.23-2.22a.8.8,0,0,1,1.16,0,.8.8,0,0,1,0,1.16L7.27,25.9A.86.86,0,0,1,6.68,26.14Z" fill="var(--primary)" /><path d="M25.32,26.14a.86.86,0,0,1-.59-.24l-2.22-2.23a.79.79,0,0,1-.25-.58.83.83,0,0,1,1.41-.58l2.23,2.22a.86.86,0,0,1,.24.59.82.82,0,0,1-.82.82Z" fill="var(--primary)" /><path d="M8.91,9.74a.83.83,0,0,1-.58-.25L6.1,7.26a.82.82,0,0,1,0-1.16.83.83,0,0,1,1.17,0L9.49,8.33a.8.8,0,0,1,0,1.16A.83.83,0,0,1,8.91,9.74Z" fill="var(--primary)" /></svg> */}
                        <svg id="f1409d26-e98e-4800-8779-746ec007425e" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M16,24.2A8.21,8.21,0,1,1,24.21,16,8.22,8.22,0,0,1,16,24.2Zm0-14A5.79,5.79,0,1,0,21.79,16,5.8,5.8,0,0,0,16,10.21Z" fill="var(--primary)"/><path d="M16,7.41A1.21,1.21,0,0,1,14.79,6.2V3.12a1.21,1.21,0,0,1,2.42,0V6.2A1.21,1.21,0,0,1,16,7.41Z" fill="var(--primary)"/><path d="M16,30.09a1.21,1.21,0,0,1-1.21-1.21V25.8a1.21,1.21,0,1,1,2.42,0v3.08A1.21,1.21,0,0,1,16,30.09Z" fill="var(--primary)"/><path d="M28.88,17.21H25.8a1.21,1.21,0,1,1,0-2.42h3.08a1.21,1.21,0,0,1,0,2.42Z" fill="var(--primary)"/><path d="M6.2,17.21H3.12a1.21,1.21,0,1,1,0-2.42H6.2a1.21,1.21,0,0,1,0,2.42Z" fill="var(--primary)"/><path d="M22.93,10.28a1.2,1.2,0,0,1-1.21-1.21,1.22,1.22,0,0,1,.35-.85L24.25,6A1.21,1.21,0,0,1,26,6a1.16,1.16,0,0,1,.35.85,1.2,1.2,0,0,1-.35.86L23.78,9.93A1.22,1.22,0,0,1,22.93,10.28Z" fill="var(--primary)"/><path d="M6.89,26.31A1.21,1.21,0,0,1,6,24.25l2.18-2.18a1.21,1.21,0,0,1,1.71,0,1.24,1.24,0,0,1,.35.86,1.2,1.2,0,0,1-.35.85L8.84,24.87h0L7.75,26A1.2,1.2,0,0,1,6.89,26.31Z" fill="var(--primary)"/><path d="M25.11,26.31a1.2,1.2,0,0,1-.86-.35l-1.09-1.09h0l-1.09-1.09a1.2,1.2,0,0,1-.35-.85,1.24,1.24,0,0,1,.35-.86,1.21,1.21,0,0,1,1.71,0L26,24.25a1.21,1.21,0,0,1-.85,2.06Z" fill="var(--primary)"/><path d="M9.07,10.28a1.22,1.22,0,0,1-.85-.35L6,7.75a1.2,1.2,0,0,1-.35-.86A1.16,1.16,0,0,1,6,6,1.21,1.21,0,0,1,7.75,6L9.93,8.22a1.22,1.22,0,0,1,.35.85,1.2,1.2,0,0,1-1.21,1.21Z" fill="var(--primary)"/><rect width="32" height="32" fill="none"/></svg>
                        Light
                    </span>
                </label>
                <input type="radio" id="dark__theme" name="themes" />
                <label htmlFor="dark__theme">
                    <span className="dark">
                        {/* <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M16.2,27.6l-.72,0A11.76,11.76,0,0,1,13.35,4.42a2.11,2.11,0,0,1,2,.56,2.3,2.3,0,0,1,.57,2.14A7.61,7.61,0,0,0,15.68,9,7.69,7.69,0,0,0,23,16.64h0a8.49,8.49,0,0,0,1.88-.17,2.22,2.22,0,0,1,2.1.64,2,2,0,0,1,.48,2A11.71,11.71,0,0,1,16.2,27.6ZM13.86,6.16h-.07a10,10,0,1,0,12,12.44.27.27,0,0,0-.07-.27.42.42,0,0,0-.4-.11,9.07,9.07,0,0,1-2.33.21A9.51,9.51,0,0,1,13.87,9a9.35,9.35,0,0,1,.3-2.35.42.42,0,0,0-.11-.42A.28.28,0,0,0,13.86,6.16Z" fill="var(--primary)" /></svg> */}
                        <svg id="a7c47ee4-f5d8-434a-840f-a07f925fa3a1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M16.2,28c-.25,0-.5,0-.75,0A12.11,12.11,0,0,1,13.27,4.1a2.53,2.53,0,0,1,2.4.68,2.69,2.69,0,0,1,.68,2.54A7.13,7.13,0,0,0,23,16.2h0A6.83,6.83,0,0,0,24.75,16a2.67,2.67,0,0,1,2.51.78,2.48,2.48,0,0,1,.58,2.41A12.07,12.07,0,0,1,16.2,28ZM13.72,6.75A9.43,9.43,0,1,0,25.2,18.67a8.77,8.77,0,0,1-2.3.22A9.9,9.9,0,0,1,13.44,9.1,10.34,10.34,0,0,1,13.72,6.75Z" fill="var(--primary)"/><rect width="32" height="32" fill="none"/></svg>
                        Dark
                    </span>
                </label>
                <span className="slider"></span>
            </div>
            {/* Gradient */}
            {/* <div className="gradient__switcher">
                <input ref={checkbox} type="checkbox" id="gradient__theme" onClick={gradientSwitch}/>
                <label htmlFor="gradient__theme">
                    <span className="gradient__span">
                        <svg id="fc36ebeb-75a5-40a9-831f-b26b18e829da" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M9.11,28a4.16,4.16,0,0,1-2.92-1.29h0a4.08,4.08,0,0,1-1.31-2.91A3.71,3.71,0,0,1,6,21.1a11.74,11.74,0,0,1,3-1.85l-1.4-1.41a2.83,2.83,0,0,1-.94-2,2.74,2.74,0,0,1,.89-1.94l.57-.58a2.74,2.74,0,0,1,2-.88h0a2.4,2.4,0,0,1,.7.11A3.24,3.24,0,0,1,11.58,11a11.44,11.44,0,0,0,2.11-4,8.1,8.1,0,0,1,1-2.15l.1-.14.13-.11a2.85,2.85,0,0,1,3.37-.13l.14.12,5.29,5.3-.06.57a5.26,5.26,0,0,1-.32,1.36l.37-.31.85-.76,1.75,1.73a2.52,2.52,0,0,1-.05,3.79L21.63,20.9a3,3,0,0,1-1.39.83,2.61,2.61,0,0,1-.65,2.94l-.58.59a2.67,2.67,0,0,1-4-.06l-1.41-1.41a12,12,0,0,1-1.86,3A3.71,3.71,0,0,1,9.11,28ZM7.89,25a1.43,1.43,0,0,0,2.15.16,12.14,12.14,0,0,0,1.47-2.56c.26-.52.5-1,.72-1.41L13,19.89l-1.25.73c-.39.23-.9.48-1.44.74a12.76,12.76,0,0,0-2.53,1.46,1.31,1.31,0,0,0-.45.95A1.74,1.74,0,0,0,7.89,25Zm5.16-5.14,3.69,3.67c.3.31.36.25.55.07l.57-.59c.19-.19.25-.25-.05-.56l-7.35-7.35a.71.71,0,0,0-.32-.22.49.49,0,0,0-.22.16l-.59.59a.62.62,0,0,0-.17.23h0s0,.11.22.32Zm.18-6.91s0,.06.14.15l6.2,6.2a1,1,0,0,0,.15.12s.07-.06.2-.19l4.6-4.58a.76.76,0,0,0,.17-.22.83.83,0,0,0-.12-.13l-.14-.14C22.86,15.37,20.15,17.22,19,16A1.42,1.42,0,0,1,19,13.92a8.2,8.2,0,0,0,2.1-3.14L16.8,6.46a.44.44,0,0,0-.27,0A8.86,8.86,0,0,0,16,7.85a13.65,13.65,0,0,1-2.54,4.72A1.29,1.29,0,0,0,13.23,12.9Z" fill="var(--primary)"/><rect width="32" height="32" fill="none"/></svg>
                        Flex
                    </span>
                </label>
                <div className="gradient__activity"></div>
            </div> */}
        </div>
    );
}

export default ThemeSwitcher;
