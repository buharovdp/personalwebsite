import { HashLink as Link } from 'react-router-hash-link';

import AppContainer from "components/AppContainer";
import ThemeSwitcher from "./ThemeSwitcher";

import "./index.scoped.css";

function Header(props) {

    return (
        <div className="header anim">
            <AppContainer>

                <div className="header__container">

                    <Link className="header__logo" to="#home">
                        <img className="header__logo__image" /> 
                        <h4>Slovy</h4>
                    </Link>


                    <ul className="header__navigation">
                        <ThemeSwitcher />

                        {props.navigation.map(
                            (item, key) => (
                                <li key={key}><Link to={item.link}>{item.title}</Link></li>
                            )
                        )}

                    </ul>

                </div>

            </AppContainer>
        </div>
    );

}

export default Header;
