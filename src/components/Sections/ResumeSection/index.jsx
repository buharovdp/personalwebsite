import AppContainer from "components/AppContainer";
import { TypeAnimation } from 'react-type-animation';

import "./index.scoped.css";
import TimeCard from "components/TimeCard";

function ResumeSection() {
    return (
        <section id="resume" className="resume anim">
            <AppContainer>
                <div className="resume__container">

                    <h3 className="resume__title">
                        Career
                    </h3>
                    <div className="resume__content">
                        <TimeCard
                            title="Skill Factory"
                            role="Test Developer"
                            years="March 2021 — Septemer 2021">
                            <p>
                                Compiled, developed, automated and tested tasks for a Java course for an online school.
                            </p>
                            <br/>
                            <p>
                                My responsibilities included:
                                <ul>
                                    <li>- Create task conditions and come up with test cases</li>
                                    <li>- Write a solution and tests for them</li>
                                    <li>- Automate the verification process using unit tests</li>
                                    <li>- Record video analysis of tasks</li>
                                </ul>
                            </p>
                        </TimeCard>

                        <div className="separator"></div>

                        <TimeCard
                            title="Ozon"
                            role="Golang Developer"
                            years="May 2024 — Present">
                            <p>
                            I am currently doing an internship at Ozon, holding the position of Go Junior developer, and together with the team we are writing several microservices.
                            </p>
                        </TimeCard>
                    </div>

                </div>
            </AppContainer >
        </section >
    );
}

export default ResumeSection;