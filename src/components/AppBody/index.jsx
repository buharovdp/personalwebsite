import "./index.scoped.css"

function AppBody(props) {
    return (
        <div className="content">
          {props.children}
        </div>

    );
}

export default AppBody;
