import "./index.scoped.css";

function Project(props) {
    return (
        <div className="content">
            <a href={props.link} className="link">
                <h2>{props.title}</h2>
            </a>

            <div className="stack">
                {props.children}
            </div>

            <p className="description">{props.description}</p>
        </div>
    );
}

export default Project;
