import "./index.scoped.css";

function TimeCard(props) {
    return (
        <div className="content">
            <h2>{props.title}</h2>
            <div className="role__description">
                <div className="role">{props.role}</div>
                <div className="dot"></div>
                <div className="years">{props.years}</div>
            </div>
            {props.children}
        </div>
    );
}

export default TimeCard;
