import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";

import MainPage from "routes/MainPage";
import NotFoundPage from "routes/NotFoundPage";

import "primereact/resources/primereact.min.css";

import "./reset.css";
import "./index.css";

const router = createBrowserRouter(
  [
    {
      path: "/",
      element: <MainPage /> 
    },
    {
      path: "/*",
      element: <NotFoundPage />
    }
  ],
  {
    basename: process.env.PUBLIC_URL
  }
);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
